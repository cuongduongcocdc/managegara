import React from "react";
import { Link } from "react-router-dom";
import "./menu.css";
export default function Menu() {
  return (
    <nav className="navbar navbar-inverse">
      <div className="container-fluid">
        <div className="navbar-header">
          <button
            type="button"
            className="navbar-toggle collapsed"
            data-toggle="collapse"
            data-target="#alignment-example"
            aria-expanded="false"
          >
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
          </button>
          <a className="navbar-brand" href="/home">
            Gara ô tô
          </a>
        </div>

        <div className="collapse navbar-collapse" id="alignment-example">
          <ul className="nav navbar-nav">
            <li className="active">
              <a href="/createBill">Tạo phiếu</a>
            </li>
            <li>
              <a href="#">Chi tiết phiếu</a>
            </li>
            <li>
              <a href="/task">Công việc</a>
            </li>
            <li>
              <a href="#">Khách hàng</a>
            </li>
            <li>
              <a href="#">Nhân viên</a>
            </li>
          </ul>
          <ul className="nav navbar-nav navbar-right">
            <li>
              <a href="#">
                <span className="glyphicon glyphicon-log-out"></span> Đăng xuất
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}
