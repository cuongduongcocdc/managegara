import React from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from './home/home'
import CreateBill from './bill/createBill'
import ManageTask from './task/manageTask'
function App() {
  return (
    <Router>
    <Switch>
      <Route exact path="/home" component={Home} />
      <Route exact path="/createBill" component={CreateBill} />
      <Route exact path="/task" component={ManageTask} />
    </Switch>
  </Router>
  );
}

export default App;
